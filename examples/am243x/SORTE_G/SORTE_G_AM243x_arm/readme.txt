V1.1 - 09/19/2023 by thomas Mauer
- based on FreeRTOS ARM empty project
- same as V1.0
V1.0 - 08/16/2022 by Thomas Leyrer
PRU driver example runs on AM243x Launchpad
- uses sysconfig for PRU and pin-mux setup
- uses noRTOS ARM empty project
- uses PRUSS driver to manage PRU config, load and run
- uses INTC and ARM interrupt
- runs on ICSS_G0 and PRU0 

