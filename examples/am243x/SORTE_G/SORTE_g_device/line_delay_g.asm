; Copyright (C) 2019 Texas Instruments Incorporated - http://www.ti.com/
;
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions
; are met:
;
; Redistributions of source code must retain the above copyright
; notice, this list of conditions and the following disclaimer.
;
; Redistributions in binary form must reproduce the above copyright
; notice, this list of conditions and the following disclaimer in the
; documentation and/or other materials provided with the
; distribution.
;
; Neither the name of Texas Instruments Incorporated nor the names of
; its contributors may be used to endorse or promote products derived
; from this software without specific prior written permission.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
; A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;
;     file:   line_delay.asm
;
;     brief:  AM654x Register Test Source
;
;
;    Version        Description                                Author
;     0.1         Created                                      Thomas Mauer

;************************************* Includes *************************************

; includes C header file to be shared with PRU assembler
   .cdecls C,NOLIST
%{
	#include "cslr_icss_g.h"
%}

; includes from \icss-g-industrial-fw\pru_fw_common
	;.include "icss_bsram_macros.h"
    ;.include "pru_constant_defines.h"
    ;.include "icss_xfer_defines.h"
    ;.include "icss_tm_macros.h"
    ;.include "pru_macros.h"

    .include "icss_regs.inc"
    .include "sorte_g_pru_device_register.inc"
    .include "sorte_g_state_define.inc"
    .include "macros.inc"
    .include "sorte_g_device_cfg.inc"
    .include "sorte_g_packet_definition.inc"
    .include "sorte_g_host_interface.inc"

	.global FN_LINE_DELAY_MASTER
	.global	TSK_LINE_DELAY_EOF


;****************************
; line delay master function
;****************************
FN_LINE_DELAY_MASTER:	;The timing master (TM) is the PRU with PRU_MASTER_PORT_FLAG clear, i.e. PRU in reverse direction
	; line delay trigger received via scratch register?
	qbbs	LINE_DELAY_MASTER_DONE, DEVICE_STATUS, PRU_MASTER_PORT_FLAG
	ldi		R0.b0, SHIFT_LINE_DELAY_TRIGGER
	xin		BANK2_ID, &TEMP_REG_1.b0, 1
	qbeq	LINE_DELAY_MASTER_DONE, TEMP_REG_1.b0, 0

	; enable TX_32 mode in TXCFG
	; pre-amble
	; send 8 byte PA
	;.if $defined(FULL_PREAMBLE)
	ldi		TX_DATA_WORD, 0x5555
	ldi		TX_DATA_WORD, 0x5555
	M_PUSH_WORD
	;.endif

	; send 4 byte pre-amble
	ldi		TX_DATA_WORD, 0x5555
	ldi		TX_DATA_WORD, 0xd555

	; 4 bytes header
	ldi32 	TEMP_REG_2, DELAY_REQ_P
	mov    	TX_DATA_WORD, TEMP_REG_2.w0
	; state change bit?
	qbne	LINE_DELAY_MASTER_LAST, TEMP_REG_1.b0, 2
	; set
	set		TEMP_REG_2.b3, TEMP_REG_2.b3, 3	;ST_STATE_CHANGE
	; initiate state change
	ldi		PROTOCOL_STATE_CNT, SYNC_STATE
	set		DEVICE_STATUS, DEVICE_STATUS, SWITCH_STATE

LINE_DELAY_MASTER_LAST:
	mov		TX_DATA_WORD, TEMP_REG_2.w2
	; end packet with crc generated by PRU
    M_CMD16 D_PUSH_CRC_MSWORD_CMD | D_PUSH_CRC_LSWORD_CMD | D_TX_EOF
	; clear scratch register
	ldi		TEMP_REG_1.b0, 0
	xout	BANK2_ID, &TEMP_REG_1.b0, 1

; receive DELAY_RESP packet
LINE_DELAY_MASTER_WAIT_BANK0:
	xin		RXL2_BANK0_ID, &R18.b0, 1
    qbeq	LINE_DELAY_MASTER_WAIT_BANK0, R18.b0, 0

	; read rx and tx time stamp - sof based
	.if $defined (PRU0)
	; use port 0, caculate difference between RX_SOF and TX_SOF
	lbco	&R2,ICSS_IEP_CONST, ICSS_IEP_CAPR0_REG, 4
	lbco	&R3,ICSS_IEP_CONST, ICSS_IEP_CAPR4_REG, 4
	.else
	; use port 1, caculate difference between RX_SOF and TX_SOF
	lbco	&R2,ICSS_IEP_CONST, ICSS_IEP_CAPR2_REG, 4
	lbco	&R3,ICSS_IEP_CONST, ICSS_IEP_CAPR5_REG, 4
	.endif
	sub		TEMP_REG_1, R2, R3

	; calculate min/max values
	lbco	&R4, ICSS_SHARED_RAM_CONST, LD_MIN, 8	; load also LD_MAX
	min		R4, R4, TEMP_REG_1
	max		R5, R5, TEMP_REG_1
	sbco	&R4, ICSS_SHARED_RAM_CONST, LD_MIN, 8	; load also LD_MAX
	; trace values
	lbco	&R4.w0, ICSS_SHARED_RAM_CONST, LD_BUFFER_PTR, 2
	sbco	&TEMP_REG_1, ICSS_SHARED_RAM_CONST, R4.w0, 4
	add		R4.w0, R4.w0, 4
	sbco	&R4.w0, ICSS_SHARED_RAM_CONST, LD_BUFFER_PTR, 2


	;sbco	&TEMP_REG_1, c24, R4, 4
	;add		R4, R4, 4
	lbco	&TEMP_REG_2, ICSS_SHARED_RAM_CONST, PORT_DELAY, 4
	; divide by n --> measurement results complete the measurement.
	;lsr		TEMP_REG_1, TEMP_REG_1, LD_SHIFT_16
	add		TEMP_REG_2,TEMP_REG_2, TEMP_REG_1
	sbco	&TEMP_REG_2, ICSS_SHARED_RAM_CONST, PORT_DELAY, 4
	M_CMD16 (D_RESET_RXFIFO | D_RX_ERROR_CLEAR )
	qbbc	LINE_DELAY_MASTER_DONE, DEVICE_STATUS, SWITCH_STATE
	; divide by 16 and store this new value into PORT_DELAY space
	lsr		TEMP_REG_2, TEMP_REG_2, LD_SHIFT_16
	sbco	&TEMP_REG_2, ICSS_SHARED_RAM_CONST, PORT_DELAY, 4
; subtract bridge delay = 320 ns
    ldi		TEMP_REG_1.w0, DELAY_FWD_TIME_320ns
    sub     TEMP_REG_2, TEMP_REG_2, TEMP_REG_1.w0
	; divide by 2
	lsr		TEMP_REG_2, TEMP_REG_2, 1
	sbco	&TEMP_REG_2, ICSS_SHARED_RAM_CONST, LINE_DELAY_NEXT, 2

LINE_DELAY_MASTER_DONE:
	RET

;***************************
; line delay slave function
;***************************
TSK_LINE_DELAY_EOF:
;FN_LINE_DELAY_SLAVE: ; The timing slave (TS) is the PRU with PRU_MASTER_PORT_FLAG set, i.e. PRU in fwd direction
	.if $defined(PRU0)
    xout    BANK0_ID, &r0, RANGE_R0_R19     ; save r0 - r19
	.else
    xout    BANK1_ID, &r0, RANGE_R0_R19     ; save r0 - r19
	.endif
	xin		RXL2_BANK0_ID, &r2, RANGE_R2_R18
LD_SLAVE_FRAME_PROCESS:
	; line delay frame received?
	qbne	LINE_DELAY_IGNORE_FRAME, R2.b2, T_LD_REQ_P2P
	; trigger line delay measurement on other PRU via scratch register
	ldi		R0.b0, SHIFT_LINE_DELAY_TRIGGER
	ldi		TEMP_REG_1.b0, 1	; trigger line delay w/o state change
	qbbc	LD_SLAVE_TRIGGER_LD, R2.b3, 3		; ST_STATE_CHANGE
	ldi		TEMP_REG_1.b0, 2	; trigger line delay with state change (last line delay measurement)
	; state change
	ldi		PROTOCOL_STATE_CNT, SYNC_STATE
	set		DEVICE_STATUS, DEVICE_STATUS, SWITCH_STATE
	; last slave needs to initiate state and port switch here
	qbbs	LD_SLAVE_TRIGGER_LD, DEVICE_STATUS, PRU_MASTER_PORT_FLAG
	; initiate port switching for last slave
	; clear scratch pad
	LDI		TEMP_REG_1.b0, 0
LD_SLAVE_TRIGGER_LD:
	xout	BANK2_ID, &TEMP_REG_1.b0, 1
LINE_DELAY_IGNORE_FRAME:
	M_CMD16 (D_RESET_RXFIFO | D_RX_ERROR_CLEAR )
FN_LINE_DELAY_SLAVE_DONE:
	.if $defined(PRU0)
    xin    BANK0_ID, &r0, RANGE_R0_R19     ; restore r0 - r19
	.else
    xin    BANK1_ID, &r0, RANGE_R0_R19     ; restore r0 - r19
	.endif
	; exit task
	xin     TM_YIELD_ID, &R0.b3,1
	nop
	nop
	halt


