//------------------------------------
//FILE: xfr2vbus_widget.h
//Purpose:  macros, etc for xfr2vbus_widget  
//------------------------------------


//-----------------DEFINES-------------------------

//XIDs
#define XFR2VBUS_XID_READ0 0x60 
#define XFR2VBUS_XID_READ1 0x61 
#define XFR2VBUS_XID_WRITE 0x62
#define XFR2VBUS_XID_WRITE0 0x62
#define XFR2VBUS_XID_WRITE1 0x63

#define RTU0_POKE_EN0_REG 0x124
#define RTU1_POKE_EN0_REG 0x12C

